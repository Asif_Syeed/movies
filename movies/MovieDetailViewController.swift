//
//  MovieDetailViewController.swift
//  movies
//
//  Created by BS-236 on 24/6/21.
//

import UIKit

class MovieDetailViewController: UIViewController {

    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var moviePoster: UIImageView!
    @IBOutlet weak var moviePop: UILabel!
    @IBOutlet weak var movieVote: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    
    var movie: Result?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        movieTitle.text = movie?.title
        moviePop.text = "Popularity: \((movie?.popularity)!)"
        movieVote.text = "Average Vote: \((movie?.voteAverage ?? nil)!)"
        movieDescription.text = movie?.overview
        
        let urlString = "https://image.tmdb.org/t/p/w342/"+(movie?.posterPath)!
        
        let url = URL(string: urlString)
        
        
        
        moviePoster.downloaded(from: url!)
        
    }

}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

